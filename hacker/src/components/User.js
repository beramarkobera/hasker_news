import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import '../css/Story.css';

class User extends Component {

    state = {
        user: {
            id: 0, 
            created: 0, 
            karma: 0, 
            about: "", 
            //submitted: []
        }
    }

    componentDidMount(){
        console.log(this.props.id)
        fetch('https://hacker-news.firebaseio.com/v0/user/' + this.props.id + '.json?print=pretty')
            .then(response => response.json())
            .then(json => {
                this.setState({
                    user: json
                })
            })

    }


    
    
    render() {
        const {id, created, karma, about} = this.state.user;
        let convertedTime = (new Date(created * 1000).toLocaleDateString('en-US')) + ' : ' +
            (new Date(created * 1000).toLocaleTimeString('en-US'));  
            
        const option = about ? "" : "optional-info";
        
        return (
            <div className="user-center">
                <div className="user-top-info">
                    <h3 className="user-id"> {id} </h3>
                    <p className="birthday"> <small><em>Created: {convertedTime}</em></small></p>
                </div>

                <div className="user-midd-info">
                    <p> Karma: <span className="main-text">{karma}</span></p>
                    <p className={option}> About: </p>
                    <div className="main-text" dangerouslySetInnerHTML={{__html: about}}></div>
                </div>

                <div className="user-bott-info">
                    <p> 
                        <em>Submitted stories </em> 
                        <span><Link to={"/stories/"+id}> Submitted stories </Link></span>
                    </p>
                </div>
            </div>
        )
    }
}

export default User;
