import React, {Component} from 'react';
import Story from './Story';
import '../css/Stories.css';

const MORE_STEP = 50;

class Stories extends Component {
    _isMounted = false;

    state = {
        keys: [],
        currentIndex: MORE_STEP
    }

    componentDidMount() {
        this._isMounted = true;
        
        if(!this.props.user) {
            fetch('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
            .then(response => response.json())
            .then(json => {
                if(this._isMounted) {
                    this.setState( prevState => {
                        return {
                            ...prevState, 
                            keys: json,
                        }
                    })
                }
                
            })        
        } else {
            fetch('https://hacker-news.firebaseio.com/v0/user/' + this.props.user + '.json?print=pretty')
            .then(response => response.json())
            .then(json => {
                this.setState( prevState => {
                    return {
                        ...prevState,
                        keys: json.submitted
                    }
                })
            })
        }
        
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleClick = () => {
        if(this.state.currentIndex+MORE_STEP < this.state.keys.length) {
            this.setState( prevState => {
                return {
                    ...prevState,
                    currentIndex: prevState.currentIndex + MORE_STEP
                }
            });
        } else {
            this.setState( prevState => {
                return {
                    ...prevState,
                    currentIndex: this.state.keys.length
                }
            });
        }
            
    }

    render() {
        const {keys, currentIndex} = this.state; 

        let pomList = keys.slice(0, currentIndex).map( k => {
            return (
                <Story id={k} key={k} />     
            )
        }) 
        return (
            <div className="stories-container">   
                {pomList} 
                <div className="more-wrapper">
                    <button className="more-stories" onClick={this.handleClick}>More stories...</button>
                </div>  
                
            </div>
        )
    }
    
}

export default Stories;
