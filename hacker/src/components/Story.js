import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import '../css/Story.css';

class Story extends Component {
    _isMounted = false;
    
    state = { 
        item: {}
    }

    componentDidMount() {
        this._isMounted =  true;
        const url = 'https://hacker-news.firebaseio.com/v0/item/' + this.props.id + '.json?print=pretty';

        fetch(url)
            .then(response => response.json())
            .then(json => {
                if(this._isMounted) {
                    this.setState({
                        item: json
                    });
                }  

            })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        if(this.state.item.type === "story") {
            const {title, url, by, score, descendants, id} = this.state.item;
            return (
                <div className="story-center">
                    <div className="story-content">
                        <p>
                            <a href={url} target='blank'> {title} </a> 
                            <span>author: </span><Link to={'/user/' + by}>{by}</Link>
                        </p>
                        <p>
                            <span>Votes: <em className="main-text">{score}</em></span> , 
                            <Link to={'/post/' + id} className="story-link"> Comments: {descendants}</Link>

                        </p>
                    </div>
                </div>
            )
        } else {
            return null;
        }       
        
    }
}

export default Story;
