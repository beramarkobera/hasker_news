import React from 'react'
import '../css/Story.css';

function Home() {
    return (
        <div className="story-center">
            
            <p> Welcome to prototype of my take on Hacker News website. </p>
            <p> I would like to point out that this is still a work in progress and there are probably some bugs that I didn't discover or anticipate, but would be happy to correct them given the opportunity. </p>
            <p>
                For any questions, please  write to: <br/>
                <a href="mailto:beramarkobera@gmail.com">beramarkobera@gmail.com</a>
            </p>
                
            
        </div>
    )
}

export default Home;
