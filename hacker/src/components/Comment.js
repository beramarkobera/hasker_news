import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../css/Comments.css';

class Comment extends Component {
	_isMounted = true;
	state = {
		item: {},
		collapse: true,
		story: {}
	}

  	componentDidMount() {
		this._isMounted =  true;

		fetch('https://hacker-news.firebaseio.com/v0/item/' + this.props.kidID + '.json?print=pretty')
			.then(response => response.json())
			.then(json => {
				if(this._isMounted) {
					this.getStory(json)
					this.setState( prevState => {
						return {
							...prevState,
							item: json
						}
					});
					
				}   
			})
	}

	componentWillUnmount() {
        this._isMounted = false;
    }
	  
	handleClick = () => {
		this.setState( prevState => {
			return {
				...prevState,
				collapse: !this.state.collapse
			}
		});
	}

	getStory = (parent) => {
		if(parent) {
			if(!parent.title) {
				fetch('https://hacker-news.firebaseio.com/v0/item/' + parent.parent + '.json?print=pretty')
				.then(response => response.json())
				.then(json => {
					this.getStory(json)
					
				})
			} else {
				this.setState( prevState => {
					return {
						...prevState,
						story: parent
					}
				})
			}
		}
	}
  
	render() {
		const {text, by, time, kids} = this.state.item;
		const {id, title} = this.state.story;
		let convertedTime = (new Date(time * 1000).toLocaleDateString('en-US')) + ' : ' +
			(new Date(time * 1000).toLocaleTimeString('en-US'));
		
		if (this.state.item.kids) {
			const divStyle = {marginLeft: '1em'};

			let colapseStyle = this.state.collapse ? ({}) : ({ display: 'none'})
			
			const kidsList = kids.map(kid => {
				return (
					<div key={kid} style={divStyle}>
						<Comment kidID={kid}/>
					</div>
				)
				});

			return (
				<div className="comment-center">
					<div className="comment-content">
						<div className="comment-center">
							<p>							
								<em>{convertedTime} , author: <Link to={'/user/' + by}>{by}</Link></em>
							</p>
							<div className="main-text" dangerouslySetInnerHTML={{__html: text}}></div>
							<div className="collapse-wrapper">
								<button onClick={this.handleClick} className="collapse-btn">
									{this.state.collapse ? '⇈ ⇈ ⇈ ⇈ ⇈ ⇈' : '⇊ ⇊ ⇊ ⇊ ⇊ ⇊'}
								</button>
							</div>
							
						</div>
						<div style={colapseStyle}>
							{kidsList}
						</div>
					</div>
				</div>
			)
		}

		const commAuthor = this.props.flag ? ("") : ("comm-author")
		return (
			<div className="comment-center">
				<div className="comment-content">
					<p>
						<span> {convertedTime} </span> 
						<span> | author:<em> <Link to={'/user/' + by}>{by}</Link></em> </span> 
						<span className={commAuthor}> | on: <em>  <Link to={'/post/' + id}> {title}</Link> </em> </span> 
					</p>
		
					<div className="main-text" dangerouslySetInnerHTML={{__html: text}}></div>
				</div>		
			</div>
		)
    }
  
}

export default Comment;