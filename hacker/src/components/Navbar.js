import React from 'react';
import { Link} from 'react-router-dom';

import '../css/Navbar.css';

function Navbar () {
    return (
        <nav className="header-holder">
            <p className="ic-tit"> <Link to='/'> <span className="icon-holder"> ⌨ </span> <span className="title-holder">HACKER NEWS</span> </Link></p>
    
            <ul className="nav-menu">
                <li className="menu-item"> <Link to='/stories'> Top Stories </Link> </li>
                <li className="menu-item"> <Link to='/newcomments'> Comments </Link> </li>
            </ul>
        </nav>
        
    )
}

export default Navbar;