import React, {Component} from 'react';
import Comment from './Comment';
import Story from './Story';
import '../css/Comments.css';

class Comments extends Component {
	_isMounted = false;
	state = {
		story: {
			kids: []
		}
	}

	componentDidMount() {
		this._isMounted =  true;
		let id = this.props.id;

		fetch('https://hacker-news.firebaseio.com/v0/item/' + id + '.json?print=pretty')
			.then(response => response.json())
			.then(json => {
				if(this._isMounted) {
						this.setState({
							story: json
						})
				}   
			})
	}

	componentWillUnmount() {
        this._isMounted = false;
    }


	render() {

		const kidsList = this.state.story.kids.map( kid => {
			return (
				<div key={kid}>
					<Comment kidID={kid} idx={this.props.id} story={this.state.story}/> 
				</div>				 
			)
		});

		return (
			<div>
				<div>
					<Story id={this.props.id}/>
				</div>
				{kidsList}
			</div>
		)
	}
}

export default Comments
