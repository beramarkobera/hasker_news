import React, { Component } from 'react';
import Comment from './Comment';

const COMM_NUM = 20;

class NewComments extends Component {
    _isMounted = false;

    state = {
        newestComms: [],
        max: 0,
        showAuthor: true
    }

    componentDidMount() {
        this._isMounted =  true; 
        fetch('https://hacker-news.firebaseio.com/v0/maxitem.json?print=pretty')
            .then(response => response.json())
            .then(json => {
                if(this._isMounted) {
                    this.setState(prevState => {
                        return {
                            ...prevState,
                            max: json
                        }
                    })
                    this.populateList(json);
                }
                              
            })
                   
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    populateList = (current) => {
        console.log(current)
        const max = current;
        for(let i=max; i>max-COMM_NUM; i--) {
            fetch('https://hacker-news.firebaseio.com/v0/item/' + i + '.json?print=pretty')
                .then(res => res.json())
                .then(js => {
                    if(js)
                        if(js.type === 'comment') {
                            this.setState( prevState => {
                                const pomComms = prevState.newestComms.slice();
                                pomComms.push(js);
                                return {
                                    ...prevState,
                                    newestComms: pomComms,
                                }

                            });
                        
                        }
                })                 
        }          
    }

    handleClick = () => {       
        this.setState(prevState => {
            return {
                ...prevState,
                max: prevState.max - COMM_NUM
            }
        });
        this.populateList(this.state.max - COMM_NUM);
    }

    render() {
        const newCommsList = this.state.newestComms.map( item => {
            return (
                <div key={item.id}>
                    <Comment kidID={item.id} flag={this.state.showAuthor}/>
                </div>
                
            );
        })

        return (
        <div>
            {newCommsList}
            <div className="more-wrapper">
                <button className="more-stories" onClick={this.handleClick}>More comments...</button>
            </div>
            
        </div>
        )
    }
}

export default NewComments;