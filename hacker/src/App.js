import React, { Component } from 'react';
import Navbar from './components/Navbar';
import Stories from './components/Stories';
import Comments from './components/Comments';
import NewComments from './components/NewComments';
import User from './components/User';

import './index.css';

//router import
import { BrowserRouter, Route} from 'react-router-dom';
import Home from './components/Home';

class App extends Component {
	render() {
		return (
			<BrowserRouter>
				<div className="App">
					<Navbar />
					<div className="content-holder">
					<Route exact path='/stories' component={Stories}/>
						<Route exact path="/" component={Home} />
						<Route exact path='/newcomments' component={NewComments}/>    
						<Route 
							path="/stories/:user_id" 
							render={(props) => <Stories user={props.match.params.user_id}/> } 
						/> 
						<Route 
							path='/post/:post_comms' 
							render={(props) => <Comments id={props.match.params.post_comms}/> } 
						/> 
						<Route 
							path='/user/:user_id'
							render={(props) => <User id={props.match.params.user_id} /> }
						/>  

					</div>
										
				</div>
			</BrowserRouter>      
		);
	}
}

export default App;
